
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { CrudComponent } from './componentes/crud/crud.component';
import { ComandaComponent } from './componentes/comanda/comanda.component';
import { ProdutoComponent } from './componentes/produto/produto.component';
import { ProdutosFormComponent } from './componentes/produto/produtos-form/produtos-form.component';
import { LoginComponent } from './componentes/login/login.component';
import { AuthGuard } from './guards/auth-guard';
import { AppComponent } from './app.component';
import { HomeComponent } from './componentes/home/home.component';

import { PaginaNaoEncontradaComponent } from './componentes/pagina-nao-encontrada/pagina-nao-encontrada.component';

const routes: Routes = [
  { path: 'fotos', canActivate: [AuthGuard], component: CrudComponent},
  { path: 'comandas', canActivate: [AuthGuard],  component: ComandaComponent},
  { path: 'produtos', canActivate: [AuthGuard],  component: ProdutoComponent},
  { path: 'produtos/editar:id', canActivate: [AuthGuard], component: ProdutosFormComponent},
  { path: 'login',  component: LoginComponent },
  { path: 'home', canActivate: [AuthGuard], component: HomeComponent },
  { path: '', canActivate: [AuthGuard], redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: PaginaNaoEncontradaComponent},
];
//canActivate: [AuthGuard],

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
