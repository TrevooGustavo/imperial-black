import { environment } from './../environments/environment';
import { Component } from '@angular/core';

import { Router } from '@angular/router';
import { AuthService } from './componentes/login/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'imperial-black';
  ambiente: string;
  
  constructor(private router: Router, private authService: AuthService, ) { 
    this.ambiente = environment.ambiente;
  }
  
  ngOnInit(): void {     
  }
  mostrarMenu(){
  if( this.authService.usuarioLogado() ){
    return true;
  }
  return false;
}

  fazerLogoff(){
    this.authService.logOut();
  }

}