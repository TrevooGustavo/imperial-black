import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

//Externos
import {NgxPaginationModule} from 'ngx-pagination';
// Services
import { AuthService } from './componentes/login/auth.service';
import { BasicAuthHtppInterceptorServiceService } from './services/basic-auth-htpp-interceptor-service.service';
//Components
import { AppComponent } from './app.component';
import { CrudComponent } from './componentes/crud/crud.component';
import { ComandaComponent } from './componentes/comanda/comanda.component';
import { ProdutoComponent } from './componentes/produto/produto.component';
import { LoginComponent } from './componentes/login/login.component';
import { AuthGuard } from './guards/auth-guard';
import { PaginaNaoEncontradaComponent } from './componentes/pagina-nao-encontrada/pagina-nao-encontrada.component';
import { HomeComponent } from './componentes/home/home.component';
import { ProdutosFormComponent } from './componentes/produto/produtos-form/produtos-form.component';
import { ProdutosListComponent } from './componentes/produto/produtos-list/produtos-list.component';


@NgModule({
  declarations: [
    AppComponent,
    CrudComponent,
    ComandaComponent,
    ProdutoComponent,
    LoginComponent,
    PaginaNaoEncontradaComponent,
    HomeComponent,
    ProdutosFormComponent,
    ProdutosListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    DataTablesModule,
    RouterModule,
    NgxPaginationModule
   


  ],
 // {provide:HTTP_INTERCEPTORS, useClass:BasicAuthHtppInterceptorServiceService, multi:true}
  providers: [HttpClient, AuthService, AuthGuard,
    {provide:HTTP_INTERCEPTORS, useClass:BasicAuthHtppInterceptorServiceService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
