import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Usuario } from './../../model/Usuario';
import { environment } from 'src/environments/environment';
import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
public usuarioAutenticado: boolean = false;

private readonly  API = `${environment.API}auth`;
message = ''
  constructor(private httpClient: HttpClient,
    private router: Router) { }

  fazerLogin(usuario: Usuario) {
    return this.httpClient.post<any>(this.API, usuario);
      }

    usuarioLogado() {
      let user = sessionStorage.getItem('token')
      //console.log(!(user === null))
    return !(user === null)
    }

    logOut() {
      sessionStorage.removeItem('token')
      sessionStorage.removeItem('username')
      this.router.navigate(['login'])
    }
}
