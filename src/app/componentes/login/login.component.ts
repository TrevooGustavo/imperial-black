import { Component, OnInit, EventEmitter } from '@angular/core';
import { AuthService } from './auth.service';
import { Usuario } from './../../model/Usuario';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
message = '';
error: any;
  public usuario: Usuario = new Usuario();
  invalidLogin = false
  constructor(private router: Router, private authService: AuthService, private userService: UserService) { }

  ngOnInit() {
  }

//   fazerLogin() {
//     if ( this.authService.fazerLogin(this.usuario) ){
//         this.router.navigate(['home'])
//         this.invalidLogin = false
        
//     } else{
//       this.invalidLogin = true
//       this.message="Usuário ou senha inválidos"
//     }
   
// }
  fazerLogin(){
  
     this.authService.fazerLogin(this.usuario).subscribe( (data: any) => {
       sessionStorage.setItem('username', this.usuario.username);
        let tokenStr= 'Bearer '+data.token;
       sessionStorage.setItem('token', tokenStr);
       this.router.navigate(['home'])
       console.log(data);
     },  error => {
      this.error = error;
      console.error("Algo deu errado ", error);
    });
   
    
  
}

}