import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { Comanda } from 'src/app/model/comanda.model';
import { Produto } from 'src/app/model/produto.model';
import { ComandaService } from './../../services/comanda.service';
import { ProdutoService } from './../../services/produto.service';

@Component({
  selector: 'app-comanda',
  templateUrl: './comanda.component.html',
  styleUrls: ['./comanda.component.css']
})
export class ComandaComponent implements OnInit {
  erro: any;
comandas: Comanda[];
produtos: Produto[];
comanda: Comanda;
showSpin: boolean = false;
message: string = '';
pagAtual: number = 1;  // referente à tabela de produtos
pAtual: number = 1; //  referente à tabela de comandas 
pageSize = 5;
 @ViewChild('dataTable', {static: true}) table: ElementRef;



  //  dtOptions: DataTables.Settings = {};
  constructor(private service: ComandaService, private pService: ProdutoService) {
  }
      ngOnInit(): void {
        this.comanda = new Comanda();
        this.buscarProdutos();
        this.buscarComandas();
      }
   criarComanda(form) {
    console.log(form.value);
    this.showSpin = true;
      this.comanda = form.value;
      this.service.criarComanda(this.comanda).subscribe( (data: any) =>{
        this.showSpin = false;
        this.message = "Cadastro realizado com sucesso !!!"
        this.comandas.push(data)
        this.comanda = new Comanda();
        this.buscarComandas();
        setTimeout(()=> this.message ='', 3000) });
   }


  buscarComandas(){
    this.service.buscarComandas().subscribe( (data: Comanda[]) =>{
      this.comandas = data;
      console.log(this.comandas);
  }, error => {
    this.erro = error;
    console.error(error);
  });
    }

  buscarProdutos(){
    this.pService.findAll().subscribe( (data: Produto[]) =>{
      this.produtos = data;
 
  }, error => {
    this.erro = error;
    console.error(error);
  });
    }

    

}
