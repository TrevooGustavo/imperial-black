import { Images } from './../../model/placeholder.model';
import { Component, OnInit } from '@angular/core';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.css']
})
export class CrudComponent implements OnInit {
  images: Images[];
  erro: any;
  constructor(private crudService: CrudService) {
    this.getFotos();
  }

  ngOnInit() {
  }

  getFotos() {
    this.crudService.getFotos().subscribe( (data: Images[]) => {
        this.images = data;
        console.log(this.images);
    }, error => {
      this.erro = error;
      console.error(error);
    });
  }

}
