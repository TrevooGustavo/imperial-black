import { Component, OnInit } from '@angular/core';


import { Comanda } from 'src/app/model/comanda.model';
import { ComandaService } from './../../services/comanda.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  comandas: Comanda[];
  erro:any;
  constructor(private service: ComandaService) { }

  ngOnInit() {
    this.buscarComandas();
  }

  buscarComandas(){
    this.service.buscarComandas().subscribe( (data: Comanda[]) =>{
      this.comandas = data;
      console.log(this.comandas);
  }, error => {
    this.erro = error;
    console.error(error);
  });
    }
}
