
import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { ProdutoService } from './../../services/produto.service';
import { Produto } from 'src/app/model/produto.model';
import { NgForm } from '@angular/forms';
declare var $;
@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.css']
})
export class ProdutoComponent implements OnInit {
produtos: Produto[]= [];
erro: any;
message = '';
produto: Produto;
showSpin = false;
pAtual:  number = 1;
@ViewChild('dataTable', {static: true}) table: ElementRef;

  constructor(private service:ProdutoService) {
    //this.findAll();
    this.produto = new Produto();
   }
  ngOnInit() {
      this.findAll();
   
  }

  findAll(){

    if(this.produtos.length === 0){
      this.service.findAll().subscribe( (data: Produto[]) =>{
        this.produtos = data;
    }, error => {
      this.erro = error;
      console.error("Algo deu errado ", error);
    });
    }
    }

    salvarProduto(form){
      this.produto = form.value;
      if(this.produto.id == null){
        this.service.save(this.produto).subscribe( (data: any) =>{
          this.produtos.push(this.produto);
          this.produtos.reverse();
          this.findAll();
            this.message = "Produto cadastrado com sucesso !!!"
            setTimeout(()=> this.message ='', 3000) });
      }else{
        console.log("Update")
        this.produto = form.value;
        this.service.save(this.produto).subscribe( (data: any) =>{
            this.message = "Produto Atualizado com sucesso !!!"
            setTimeout(()=> this.message ='', 3000) });
            
            this.produto = new Produto();
            location.reload();
      }
        }
        //this.findAll();
        
        onPreUpdate(produto: Produto){
          this.produto = Object.assign({}, produto)
        }

        limpaForm(produtoForm?: NgForm){
      this.produto = new Produto();
        
      }

      // onPreDelete(produto: Produto){
      // return this.produto = Object.assign({}, produto);
      // }
        onDelete(produto: Produto){
          this.service.delete(this.produto).subscribe( (data: any) =>{
            this.message = "Produto deletado com  sucesso !!!"
            setTimeout(()=> this.message ='', 3000) });
            
         location.reload();
        }
    }