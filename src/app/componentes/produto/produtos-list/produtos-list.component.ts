import { Component, OnInit } from '@angular/core';

import { ProdutoService } from './../../../services/produto.service';
import { Produto } from 'src/app/model/produto.model';
@Component({
  selector: 'app-produtos-list',
  templateUrl: './produtos-list.component.html',
  styleUrls: ['./produtos-list.component.css']
})
export class ProdutosListComponent implements OnInit {
  produtos: Produto[] =  [];
  erro: any;
  constructor(private service: ProdutoService) { }

  ngOnInit() {
    this.findAll();
  }

  findAll(){
    if(this.produtos.length === 0){
      this.service.findAll().subscribe( (data: Produto[]) =>{
        this.produtos = data;
        console.log(this.produtos);
    }, error => {
      this.erro = error;
      console.error("Algo deu errado ", error);
    });
    }
    }
}
