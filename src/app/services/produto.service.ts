import { environment } from 'src/environments/environment';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Produto } from './../model/produto.model';


@Injectable({
  providedIn: 'root'
})
export class ProdutoService {
private readonly  API = `${environment.API}produto`
//private delete: string = '/delete';
  constructor(private http: HttpClient) { }

  public findAll(): Observable<Produto[]> {
    return this.http.get<any>(this.API);
   }

  public save(produto: Produto): Observable<Produto> {
    return this.http.post<any>(this.API,produto);
    }

     public delete(produto: Produto): Observable<void> {
       return this.http.post<any>(this.API+'/delete', produto);
     }
}
