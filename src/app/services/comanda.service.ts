import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Comanda } from '../model/comanda.model';
import { Produto } from './../model/produto.model';


@Injectable({
  providedIn: 'root'
})
export class ComandaService {
private comanda: Comanda;
private readonly  API = `${environment.API}comandas`

  constructor(private http: HttpClient) { }

  criarComanda(comanda): Observable<Comanda> {
    return this.http.post<any>(this.API, comanda);
  }
  buscarComandas(): Observable<Comanda[]> {
    return this.http.get<any>(this.API);
  }
  public buscarProdutos(): Observable<Produto[]> {
    return this.http.get<any>('');
          }
}
