import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Usuario } from './../model/Usuario';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
usuario: Usuario = new Usuario();
  constructor( private httpClient: HttpClient) { }
  private readonly  API = `${environment.API}user`;
  getUsers(){
    let  userName = 'guga.tavoo@gmail.com'
    let password= '123456'

    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(userName + ':' + password) });
    return this.httpClient.get<Usuario[]>(this.API,{headers});
  }
}
