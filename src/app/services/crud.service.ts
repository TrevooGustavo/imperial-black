import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Images } from './../model/placeholder.model';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private http: HttpClient) { }


  public getFotos(): Observable<Images[]> {
    return this.http.get<any>('https://jsonplaceholder.typicode.com/photos');
          }
}
