import { Produto } from './produto.model';

export class Comanda {
  id: number;
  mesa: number;
  cliente: string;
  status: string = "Ativa";
  produto: Produto[];
  qtdProduto: number;
  valorTotal: number;

}
